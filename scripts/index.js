import './components/preloader';
import './components/menu';
import './components/header';
import './components/timeline';
import './components/tabs';
import './components/sheme';
import './components/parallax';
import './components/documents';
import './components/partners';
import './components/slider';
import './components/activitiesSlider';
import './components/downloads';
import './components/sequention';
import './components/applying';
import './components/scrollBtn';
import './components/categoriesParallax';
import './components/form';

/**
 * ACTION PLUGINS
 */

// action fancybox
$('[data-fancybox="images"]').fancybox({});

$('[data-fancybox]').fancybox({
    youtube : {
        controls : 0,
        showinfo : 0
    },
    vimeo : {
        color : 'f00'
    }
});

// action inputmask
Inputmask({"mask": "+7 (999) 999-9999"}).mask('.input-phone');

document.querySelectorAll('.a').forEach(el => {
  el.onclick = function(e) {
    if (e.altKey) {
      if (e.shiftKey) {
        window.open(el.getAttribute('href'), "_blank");
      } else {
        window.location.href = el.getAttribute('href');
      }
    }
  }
});

// toastr

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

// lazy loading
document.addEventListener("DOMContentLoaded", function() {
  var lazyImages = [].slice.call(document.querySelectorAll("img[data-src]"));

  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          if (lazyImage.dataset.src) lazyImage.src = lazyImage.dataset.src;
          if (lazyImage.dataset.srcset) lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.removeAttribute('data-src');
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  } else {
    lazyImages.forEach(lazyImage => {
      lazyImage.setAttribute('src', lazyImage.getAttribute('data-src'));
      lazyImage.onload = function() {
        lazyImage.removeAttribute('data-src');
      };
    })
  }
});

document.addEventListener("DOMContentLoaded", function() {
  var lazyBackgrounds = [].slice.call(document.querySelectorAll(".lazy-background"));

  if ("IntersectionObserver" in window) {
    let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          entry.target.classList.add("visible");
          lazyBackgroundObserver.unobserve(entry.target);
        }
      });
    });

    lazyBackgrounds.forEach(function(lazyBackground) {
      lazyBackgroundObserver.observe(lazyBackground);
    });
  } else {
    lazyBackgrounds.forEach(lazyBackground => {
      lazyBackground.classList.add("visible");
    })
  }
});

document.addEventListener("DOMContentLoaded", function() {
  var lazyVideos = [].slice.call(document.querySelectorAll("video.lazy"));

  if ("IntersectionObserver" in window) {
    var lazyVideoObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(video) {
        if (video.isIntersecting) {
          for (var source in video.target.children) {
            var videoSource = video.target.children[source];
            if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
              videoSource.src = videoSource.dataset.src;
            }
          }

          video.target.load();
          video.target.classList.remove("lazy");
          lazyVideoObserver.unobserve(video.target);
        }
      });
    });

    lazyVideos.forEach(function(lazyVideo) {
      lazyVideoObserver.observe(lazyVideo);
    });
  }
});


