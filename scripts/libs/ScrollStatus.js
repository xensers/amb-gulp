export default class ScrollStatus {
  constructor(el) {
    this.target = el;
  }

  get progress() {
    const { bottom, height } = this.target.getBoundingClientRect();
    let progress = 1 - (bottom / (window.innerHeight * (height / window.innerHeight + 1)));

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get isView(){
    return this.progress > 0 && this.progress < 1
  }

  get enterProgress() {
    const { top, height } = this.target.getBoundingClientRect();
    let progress = 1 - top / window.innerHeight;

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get isEnter() {
     return this.enterProgress > 0 && this.enterProgress < 1
  }

  get leaveProgress() {
    const { bottom } = this.target.getBoundingClientRect();
    let progress = 1 - bottom / window.innerHeight;

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get isLeave() {
     return this.leaveProgress > 0 && this.leaveProgress < 1
  }

  get screenViewProgress() {
    const rect = this.target.getBoundingClientRect();
    const { bottom, height } = rect;
    let progress = 1 - (bottom - window.innerHeight) / (height - window.innerHeight);

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }
}
