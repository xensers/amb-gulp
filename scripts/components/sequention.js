import ScrollStatus from "../libs/ScrollStatus";
import addZero from "../helpers/addZero";

class Sequention {
  constructor(elContainer, { path, prefix, extension, digitsLength, count }) {

    this._active = 0;
    this._progress = 0;

    this.el           = elContainer;
    this.path         = path;
    this.prefix       = prefix;
    this.extension    = extension;
    this.count        = parseInt(count);
    this.digitsLength = parseInt(digitsLength);

    this.frames = this.createFrames();

  }

  createFrames() {
    let frames = [];
    this.el.innerHTML = '';

    for (let i = 0; i <= this.count; i++) {
      let frame = {};

      let img = new Image();
      let src = this.path + this.prefix + addZero(this.digitsLength, i) + '.' + this.extension;

      img.classList.add('sequention__frame');
      img.src = src;

      this.el.appendChild(img);

      frames[i] = img;
    }

    frames[this._active].classList.add('sequention__frame_active');

    return frames;
  }

  set active(val) {
    const prev = this._active;
    if (val === prev) return false;

    this._active = val;
    if (val > this.count) this.active = this.count;
    if (val < 0) this.active = 0;

    requestAnimationFrame(() => {
      this.frames[prev].classList.remove('sequention__frame_active');
      this.frames[this._active].classList.add('sequention__frame_active');
    });
  }

  get active() {
    return this._active;
  }

  set progress(val) {
    this._progress = val;
    if (this._progress > 1) this._progress = 1;
    if (this._progress < 0) this._progress = 0;
    this.active = parseInt(val * this.count);
  }

  get progress() {
    return this._progress;
  }
}



document.querySelectorAll('.scatter').forEach(elScatter => {
  const elSequention = elScatter.querySelector('.sequention');
  const params = elSequention.dataset;

  const sequention = new Sequention(elSequention, params);

  const scrollStatus = new ScrollStatus(elScatter);

  window.addEventListener('scroll', e => {
    const { screenViewProgress } = scrollStatus;

    sequention.progress = screenViewProgress;
  });

});
