document.querySelectorAll('.partners').forEach(elPartners => {
  const $partners = $(elPartners);
  const $carousel = $partners.find(".partners-slider .owl-carousel");
  const $pagination = $partners.find('.carousel-pagination');
  const $paginationIndex = $pagination.find('.carousel-pagination__index');
  const $paginationProgress = $pagination.find('.carousel-pagination__progress');
  const $paginationCount = $pagination.find('.carousel-pagination__count');

  const cbUpdatePagination = function(e) {
    let count = e.page.count;
    let index = e.page.index;
    let progress = index / (count-1);

    $paginationIndex.html(index + 1);
    $paginationCount.html(count);
    $paginationProgress.css({
      transform: `translateX(-${(1 - progress) * 100}%)`
    });

    if (progress >= 1) {
      $paginationCount.css({
        color: '#C10230'
      })
    } else {
      $paginationCount.css({
        color: ''
      })
    }
  };

  $carousel.owlCarousel({
    loop: true,
    navText: ['', ''],
    dots: true,
    navContainer: $partners.find('.owl-nav'),
    autoWidth: true,
    margin: 20,
    // autoplay: true,
    autoplayTimeout: 2000,
    items: 6,
    autoplayHoverPause: true,
    onChanged: cbUpdatePagination,
    onInitialized: function() {
      // Костыль от обновления нумерации страниц после инициализации
      // событие onInitialized выдает не те данные
      setTimeout(() => {
        this.next();
        setTimeout(() => {
          this.prev();
        })
      })
    }
  });
});
