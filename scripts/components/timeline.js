document.querySelectorAll('.history').forEach(elementHistory => {
  const elementTimeline = elementHistory.querySelector('.timeline');
  const elSlider = elementHistory.querySelector('.history-slider');
  const elSliderWrp = elementHistory.querySelector('.history-slider__wrp');
  const elPrev = elementHistory.querySelector('.history-controls__prev');
  const elNext = elementHistory.querySelector('.history-controls__next');

  const years = {};
  const yearsArr = [];
  const slides = [];

  let activeSlideIndex = 0;
  let activeSlide = null;
  let activeYearKey = null;
  let activeYearIndex = 0;
  let activeYear = null;

  let timerWrpOut = null;
  let timerWrpOver = null;

  // methods
  function setActiveYear(newYear) {
    activeYearKey = newYear;
    activeYearIndex = yearsArr.findIndex(item => item.year === activeYearKey);
    activeYear = years[activeYearKey];

    if (activeSlide.year !== newYear) {
      setActiveSlide(activeYear.firstSlideIndex);
    }

    for (let key in years) {
      years[key].el.classList.remove('timeline__item_active');
    }
    years[activeYearKey].el.classList.add('timeline__item_active');

    if (window.innerWidth < 780) {
      yearsArr.forEach((item, i) => {
        let left = -40 * activeYearIndex + 10;
        if (activeYearIndex === 0) left -= 10;
        if (activeYearIndex === yearsArr.length - 1) left += 40;

        item.el.style.minWidth = '40%';
        item.el.style.left = left + '%';
      });
    }

  }

  function setActiveSlide(index) {
    if (index < 0) return false;
    if (index >= slides.length) return false;

    const oldActiveSlideIndex = activeSlideIndex;
    activeSlide = slides[index];
    activeSlideIndex = index;
    setActiveYear(activeSlide.year);

    slides.forEach((slide, index) => {
      slide.el.classList.remove('history-slider__item_exact-active');
      slide.el.classList.remove('history-slider__item_active')
      if (activeSlide.year === slide.year) slide.el.classList.add('history-slider__item_active');
    });
    activeSlide.el.classList.add('history-slider__item_exact-active');


    requestAnimationFrame(() => {
      let offset = 0;
      let i = 0;
      if (window.innerWidth >= 760) i = 1;
      for (; i < index; i++) {
        offset += slides[i].el.offsetWidth;
      }

      if (activeSlideIndex > oldActiveSlideIndex) {
        for (let i = 0; i < 5; i++) {
          let s = index - 2 + i;
          if (s >= 0 && s < slides.length) {
            slides[s].el.style.transitionDelay = i * 60 + 'ms';
          }
        }
      } else {
        for (let i = 5; i > 0; i--) {
          let s = index + 2 - i;
          if (s >= 0 && s < slides.length) {
            slides[s].el.style.transitionDelay = i * 60 + 'ms';
          }
        }
      }

      requestAnimationFrame(() => {
        slides.forEach(slide => {
          slide.el.style.transform = `translateX(-${ offset }px)`;
          requestAnimationFrame(() => {
            slide.el.style.transitionDelay = '';
          })
        })
      })
    })
  }

  // init
  [...elementHistory.querySelectorAll('.history-slider__item')].map((elSlide, index) => {
    const year = elSlide.querySelector('.history-card__year').innerText;

    // init slides
    elSlide.onclick = () => setActiveSlide(index);
    slides.push({
      el: elSlide,
      year,
      isActive: false
    });


    // init years
    if (!years.hasOwnProperty(year)) {
      const elYear = document.createElement('div');
      elYear.classList.add('timeline__item');
      elYear.innerHTML = `
          <div class="timeline__item-trigger">
            <div class="timeline__item-circle"></div>
            <div class="timeline__item-label">${year}</div>
          </div>
      `;

      elementTimeline.appendChild(elYear);

      const yearItem = {
        el: elYear,
        year,
        firstSlideIndex: index,
        slidesIndexes: [index],
        count: 1
      };

      elYear.onclick = () => {
        if (yearItem.count > 2 && window.innerWidth > 760) {
          setActiveSlide(yearItem.firstSlideIndex + 1);
        } else {
          setActiveSlide(yearItem.firstSlideIndex);
        }
      };

      years[year] = yearItem;
      yearsArr.push(yearItem);
    } else {
      years[year].count++;
      years[year].slidesIndexes.push(index);
    }
  });

  setActiveSlide(0);


  // events
  if (window.innerWidth > 1240) {
    elSliderWrp.style.transition = 'transform 0.2s ease';
    elSlider.onmouseover = e => {
      clearTimeout(timerWrpOut);
      clearTimeout(timerWrpOver);

      timerWrpOver = setTimeout(() => {
        requestAnimationFrame(() => {
          elSliderWrp.style.transition = '';

          elSlider.onmousemove = e => requestAnimationFrame(() => {
            let progress = 1 - e.clientX / window.innerWidth;
            elSliderWrp.style.transform = `translateX(${ -50 + progress * 100 }px)`
          });
        });
      }, 300);
    };

    elSlider.onmouseout = e => {
      clearTimeout(timerWrpOut);
      clearTimeout(timerWrpOver);

      timerWrpOut = setTimeout(() => {
        elSliderWrp.style.transition = 'transform 0.2s ease';
        requestAnimationFrame(() => {
          elSliderWrp.style.transform = '';
        })
      }, 100)
    };
  }

  elNext.onclick = () => {
    if (window.innerWidth > 760) {
      let index = yearsArr.findIndex(item => item.year === activeYearKey) + 1;

      if (index < yearsArr.length) {
        if (yearsArr[index].count > 2) {
          setActiveSlide(yearsArr[index].firstSlideIndex + 1);
        } else {
          setActiveSlide(yearsArr[index].firstSlideIndex)
        }
      }
    } else {
      setActiveSlide(activeSlideIndex + 1);
    }
  };

  elPrev.onclick = () => {
    if (window.innerWidth > 760) {
      let index = yearsArr.findIndex(item => item.year === activeYearKey) - 1;
      if (index >= 0) {
        if (yearsArr[index].count > 2) {
          setActiveSlide(yearsArr[index].firstSlideIndex + 1);
        } else {
          setActiveSlide(yearsArr[index].firstSlideIndex)
        }
      }
    } else {
      setActiveSlide(activeSlideIndex - 1);
    }
  }
});
