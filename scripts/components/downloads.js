document.querySelectorAll('.downloads').forEach(elDownloads => {
  let isAccordion = true;

  const elmsTabs = elDownloads.querySelectorAll('.downloads__tab');
  const elmsTabHeaders = [];
  const elHeader = document.createElement('div');
  elHeader.className = 'downloads__header';

  if (!!elmsTabs.length && window.innerWidth > 760) {
    elmsTabs[0].classList.add('downloads__tab_active')
  }

  elmsTabs.forEach(elTab => {
    const elTabHeader = elTab.querySelector('.downloads__tab-header');
    elmsTabHeaders.push(elTabHeader);

    if (elTab.classList.contains('downloads__tab_active')) elTabHeader.classList.add('downloads__tab-header_active');

    elTabHeader.onclick = e => {
      let tabIsActive = elTab.classList.contains('downloads__tab_active');
      elmsTabs.forEach(el => el.classList.remove('downloads__tab_active'));
      elmsTabHeaders.forEach(el => el.classList.remove('downloads__tab-header_active'));

      if (!isAccordion || !tabIsActive) {
        elTab.classList.add('downloads__tab_active');
        elTabHeader.classList.add('downloads__tab-header_active');
      }
    };

  });

  const resize = () => {
    if(window.innerWidth > 760) {
      if(isAccordion) {
        elmsTabHeaders.forEach(elTabHeader => elHeader.append(elTabHeader));
        elDownloads.prepend(elHeader);
      }
      isAccordion = false;
    } else {
      if(!isAccordion) {
        elmsTabHeaders.forEach((elTabHeader, index) => elmsTabs[index].prepend(elTabHeader));
        elHeader.remove();
      }
      isAccordion = true;
    }
  };

  resize();
  window.addEventListener('resize', resize);
});
