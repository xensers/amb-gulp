class Tabs {
  constructor(el) {
    this.target   = el;
    this.navItems = [...el.querySelectorAll('.tabs__nav-item')];
    this.navLinks = [...el.querySelectorAll('.tabs__nav-link')];
    this.items    = [...el.querySelectorAll('.tabs__item')];

    this.navLinks.forEach(item => item.addEventListener('click', this.navClickHandler.bind(this)));
  }

  navClickHandler(e) {
    e.preventDefault();

    const hash = e.target.hash;

    this.navItems.forEach(item => item.classList.remove('tabs__nav-item_active'));
    this.items.forEach(item => {
      if('#' + item.id === hash) {
        item.classList.add('tabs__item_active');
      } else {
        item.classList.remove('tabs__item_active');
      }
    });
    e.target.parentElement.classList.add('tabs__nav-item_active');

    return false;
  }
}

// Action tabs
document.querySelectorAll('.tabs').forEach(el => new Tabs(el));
