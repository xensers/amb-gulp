import ScrollStatus from "../libs/ScrollStatus";

document.querySelectorAll('.category__image').forEach(el => {
  const scrollStatus = new ScrollStatus(el);
  const getY = () => {
    if (window.innerWidth < 760) return 200 * scrollStatus.progress * -1 + 50;
    return 500 * scrollStatus.progress * -1 + 150
  };
  const render = () => {
    const y = getY();
    el.style.transform = `translateY(${ y }px)`;
  };

  requestAnimationFrame(render);

  window.addEventListener('scroll', e => {
    requestAnimationFrame(render);
  })
});
