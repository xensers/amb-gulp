class Scheme {
  constructor(el, params) {
    this._el = el;
    this._params = params;

    let lastBreakpoint = this.breakpoint;

    window.addEventListener('resize', () => {
      let breakpointIsChange = lastBreakpoint !== this.breakpoint;

      if (breakpointIsChange) {
        lastBreakpoint = this.breakpoint;
        this.render();
      }
    });

    this.render();
  }

  labelHandler(e, stage) {
    this._el.querySelectorAll('.scheme__image-stage').forEach(el => {
      if(el.dataset.stage === stage) {
        el.classList.add('scheme__image-stage_active');
      } else {
        el.classList.remove('scheme__image-stage_active');
      }
    });

    this._lables.forEach(el => {
      if(el.dataset.stage === stage) {
        el.classList.add('scheme__label_active');
      } else {
        el.classList.remove('scheme__label_active');
      }
    })
  }

  get breakpoint() {
    if (window.innerWidth > 1600) {
      return 'desktop';
    } else if (window.innerWidth > 1240) {
      return 'laptop'
    } else if (window.innerWidth > 760) {
      return 'tablet'
    }
    return 'mobile';
  }


  async render() {
    let stagesHTML =  '';

    this._params.stages.forEach((src, index) => {
      stagesHTML += `<img class="scheme__image-stage ${index === 0 ? 'scheme__image-stage_active' : ''}" data-stage="${index}" src="${src}">\n`
    });

    let labels = this._params.labels[this.breakpoint];
    let labelsHTML = `<div class="scheme__labels" style="margin-top: ${ labels.y };margin-left: ${ labels.x }"></div>`;

    this._el.innerHTML = `
      <div class="scheme__main">
        <img class="scheme__placeholder" src="${this._params.placeholder}" alt="" >
        ${stagesHTML}
      </div>
      ${labelsHTML}
      <div class="scheme__description">
         <p>${this._params.description}</p>
      </div>
    `

    this.elLabels = this._el.querySelector('.scheme__labels');

    let labelsResponse = await fetch(labels.src);
    let labelsSVG = await labelsResponse.text();

    this.elLabels.innerHTML = labelsSVG;

    this._lables = [...this._el.querySelectorAll('.scheme__label')];
    this._lables.forEach(label => {
      label.onmouseover = e => this.labelHandler(e, label.dataset.stage);
      label.onmouseout = e => this.labelHandler(e, '0');
    });
  }
}



/**
 *  ACTION SCHEMES
 */

document.querySelectorAll('.scheme-gas').forEach(elScheme => {
  new Scheme(elScheme, {
    placeholder : '/images/schemes/gas/stage-0.svg',
    stages : [
      '/images/schemes/gas/stage-0.svg',
      '/images/schemes/gas/stage-1.svg',
      '/images/schemes/gas/stage-2.svg',
      '/images/schemes/gas/stage-3.svg',
      '/images/schemes/gas/stage-4.svg'
    ],
    labels: {
      'desktop': { src: '/images/schemes/gas/labels/desktop.svg', x: '21px',  y: '82px' },
      'laptop':  { src: '/images/schemes/gas/labels/laptop.svg',  x: '-5px',  y: '55px' },
      'tablet':  { src: '/images/schemes/gas/labels/tablet.svg',  x: '0px',   y: '71px' },
      'mobile':  { src: '/images/schemes/gas/labels/mobile.svg',  x: '-10px', y: '-53px' },
    },
    description: 'Мы используем собственные высокочувствительные полупроводниковые сенсоры SS–GAS с уникальным алгоритмом математической обработки результатов измерений, что позволяет значительно улучшить селективность, чувствительность и быстродействие наших изделий.'
  })
});

document.querySelectorAll('.scheme-teplo').forEach(elScheme => {
  new Scheme(elScheme, {
    placeholder : '/images/schemes/teplo/stage-0.svg',
    stages : [
      '/images/schemes/teplo/stage-0.svg',
      '/images/schemes/teplo/stage-1.svg',
      '/images/schemes/teplo/stage-2.svg',
      '/images/schemes/teplo/stage-3.svg',
      '/images/schemes/teplo/stage-4.svg'
    ],
    labels: {
      'desktop': { src: '/images/schemes/teplo/labels/desktop.svg', x: '0px',   y: '0px' },
      'laptop':  { src: '/images/schemes/teplo/labels/laptop.svg',  x: '-32px', y: '-46px' },
      'tablet':  { src: '/images/schemes/teplo/labels/tablet.svg',  x: '4px',   y: '-115px' },
      'mobile':  { src: '/images/schemes/teplo/labels/mobile.svg',  x: '18px',  y: '-152px' },
    },
    description: 'При производстве тепловизоров применяем материалы высокого качества и надежные комплектующие, чтобы обеспечить максимальную отказоустойчивость и долговечность эксплуатации.'
  })
});
