class Menu {
  constructor(el) {
    this._el = el;
    this._list = this._el.querySelector('.menu__list');
    this._items = [...this._list.children];
    this._items.forEach(item => item.addEventListener('click', this.itemClickHandler.bind(this, event, item)));
  }

  itemClickHandler(e, item) {
    this._items.forEach(item => {
      item.classList.remove('menu__item_open');
    });
    item.classList.add('menu__item_open');
  }

  static toggle() {
    document.documentElement.classList.toggle('menu--open')
  }

  static open() {
    document.documentElement.classList.add('menu--open')
  }

  static close() {
    document.documentElement.classList.remove('menu--open')
  }
}

// Action menu
document.querySelectorAll('.menu').forEach(elMenu => {
  const menu = new Menu(elMenu);

  document.querySelectorAll('.menu-burger').forEach(elBurger => {
    elBurger.addEventListener('click', Menu.toggle);
  });

  window.addEventListener('popstate', function (e) {
      Menu.close();
  })
});
