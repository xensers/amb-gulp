$('[data-js-documents-slider]').each((function () {
  const index = $(this).data('slider-index');

  $(this).find('.owl-carousel').owlCarousel({
    loop: false,
    nav: true,
    navText: ['', ''],
    navContainer: `[data-slider-index="${index}"] .owl-nav`,
    dots: false,
    margin: 20,
    responsive : {
      0: { items: 1, },
      780: { items: 2 },
      1240: { items: 3 },
      1600: { items: 4 },
    }
  });
}));
