document.querySelectorAll('.applying__item').forEach((elItem, index) => {
  const elImg = elItem.querySelector('.applying__img img');
  const src = elImg.src;
  const srcHover = elImg.dataset.srcHover;

  let elImgHover = new Image();
  let imgAvailable = false;
  elImgHover.src = srcHover;
  elImgHover.onload  = () => imgAvailable = true;

  let timer;

  const play = e => {
    if (imgAvailable && elImg.src !== elImgHover.src) elImg.src = elImgHover.src
  };

  const pause = e => {
    if (elImg.src !== src) elImg.src = src;
  };

  let hoverTimer;
  let isHover = false;

  elImg.onmouseover = e => {
    isHover = true;
    clearTimeout(hoverTimer);
    hoverTimer = setTimeout(play, 300);
  };
  elImg.onmouseout = e => {
    isHover = false;
    clearTimeout(hoverTimer);
    hoverTimer = setTimeout(pause, 300);
  };

  let isView = false;
  let isViewOld = false;

  window.addEventListener('scroll', e => {
    const rect = elItem.getBoundingClientRect();
    isViewOld = isView;
    isView = rect.top - window.innerHeight + 300 < 0;

    if (isView !== isViewOld && isView) {
      play();
      clearTimeout(timer);
      timer = setTimeout(() => {
        if (!isHover) pause();
      }, 2500);
    }
  })
});
