document.querySelectorAll('.activities-slider').forEach(elSlider => {
  const elementsSlides = elSlider.querySelectorAll('.activities-slider__slide');
  const elPrev         = elSlider.querySelector('.activities-slider__prev');
  const elNext         = elSlider.querySelector('.activities-slider__next');
  const elPagination   = elSlider.querySelector('.carousel-pagination');
  const elIndex        = elSlider.querySelector('.carousel-pagination__index');
  const elProgress     = elSlider.querySelector('.carousel-pagination__progress');
  const elCount        = elSlider.querySelector('.carousel-pagination__count');

  let intervar;
  let duration = 5000;
  let activeSlide = [...elementsSlides].findIndex(el => el.classList.contains('activities-slider__slide_active'));

  if (activeSlide < 0) {
    activeSlide = 0;
  }

  const updatePagination = () => {
    let progress = activeSlide / (elementsSlides.length - 1);

    elIndex.innerHTML = activeSlide + 1;
    elCount.innerHTML = elementsSlides.length;


    elProgress.style.transform = `translateX(-${(1 - progress) * 100}%)`;

    if (progress >= 1) {
      elPagination.classList.add('carousel-pagination_full');
    } else {
      elPagination.classList.remove('carousel-pagination_full');
    }
  }

  updatePagination();

  const switchSlide = index => {
    let prevSlide = activeSlide;

    activeSlide = index;
    if (activeSlide >= elementsSlides.length) activeSlide = 0;
    if (activeSlide < 0) activeSlide = elementsSlides.length - 1;


    elementsSlides.forEach(el => el.classList.remove('activities-slider__slide_active'));
    elementsSlides[activeSlide].classList.add('activities-slider__slide_active');


    updatePagination();

    clearInterval(intervar);
    intervar = setInterval(() => switchSlide(activeSlide + 1), duration);
  };

  intervar = setInterval(() => switchSlide(activeSlide + 1), duration);

  elNext.onclick = () => switchSlide(activeSlide + 1);
  elPrev.onclick = () => switchSlide(activeSlide - 1);
});
