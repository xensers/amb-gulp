document.querySelectorAll('.scroll-btn').forEach( el => {
  el.onclick = function (e) {
    window.scrollBy({
      top: window.innerHeight,
      left: 0,
      behavior: 'smooth'
    });
  }
});

document.querySelectorAll('.category__btn').forEach( elBtn => {
  elBtn.onclick = () => {
    const elTarget = document.querySelector('.category-products');

    if (elTarget) {
      window.scrollBy({
        top: elTarget.offsetTop,
        left: 0,
        behavior: 'smooth'
      });
    }
  }
});
