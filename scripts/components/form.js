//E-mail Ajax Send
$('form.contacts-form').submit(function() { //Change
  const th = $(this);

  const formDataArray = th.serializeArray();
  let check = formDataArray.find(item => item.name === 'check');

  if (!check || check.value !== 'on') {
    toastr.info('Подтвердите Ваше согласие с положением об обработке персональных данных');
    return false;
  }


  $.ajax({
    type: "POST",
    url: "/mail.php", //Change
    data: th.serialize()
  }).done(function() {
    toastr.success('Спасибо за заявку, мы ответим Вам в ближайшее время');

    setTimeout(function() {
      // Done Functions
      th.trigger("reset");
    }, 1000);
  }).fail(function() {
    toastr.error('Отправка не удалась.');
    toastr.info('Произошла какая то ошибка во время отпраки вашего сообщения. Попробуйте поврорить попытку позже или воспользуйтесь другим способом связи.');
  });
  return false;
});
