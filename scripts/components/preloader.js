document.querySelectorAll('.preloader').forEach(elPreloader => {
  const onload = e => {
    elPreloader.classList.add('preloader--loaded');
    setTimeout(() => {
      elPreloader.style.display = 'none';
    }, 1000);
  };

  window.addEventListener('load', onload);
  setTimeout(onload, 20000);
});
