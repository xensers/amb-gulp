import ScrollStatus from '../libs/ScrollStatus';

document.querySelectorAll('.parallax').forEach(elParallax => {
  const parallaxBg = elParallax.querySelector('.parallax__bg');
  const parallaxSvg = elParallax.querySelector('svg');
  const scrollStatusParallax = new ScrollStatus(elParallax);


  window.addEventListener('scroll', e => {
    const progress = scrollStatusParallax.progress;

    if (parallaxBg)  parallaxBg.style.transform = `translateY(${ 25 - (1 - progress) * 50 }%)`;
    if (parallaxSvg) parallaxSvg.style.transform = `translateY(${ 25 - (progress) * 50 }%)`;
  });
});


$('.documents-accordeon-header').click(function () {
  const hasnt = ! $(this).hasClass('documents-accordeon-header--active');

  $('.documents-accordeon-header').removeClass('documents-accordeon-header--active');

  if (hasnt) {
    $(this).addClass('documents-accordeon-header--active');
  }
});
