export default function addZero(digitsLength, source){
    let text = source + '';
    while(text.length < digitsLength) {
      text = '0' + text;
    }
  return text;
}
